---?

# Introduction to Temporal Tables

![Temporal Table Workflow](1/assets/image/temporal-table-workflow.png)

+++

## What are Temporal Tables

* They are system versioned tables that keep full history/audit of all data changes done to a table |
* Its an alternative to trigger based history/audit tables |
* Its only available in SQL Server 2016+ and SQL Azure |
* They useful for recovering from accidental data changes and application errors |

+++

## How Do Temporal Tables Work

* You need to add two additional DATETIME2 columns to a table in order to use Temporal Tables to handle the lifespan of a row |
* Any updates or deletes to the table is automatically written into a history table |
* You can use the new "FOR SYSTEM_TIME" syntax to query the data or you can interact with the history table directly |

+++