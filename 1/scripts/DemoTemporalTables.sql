-- Setup Database
BEGIN 
    USE master;
    IF EXISTS(SELECT 1 FROM sys.databases WHERE name = 'Demo')
    BEGIN
        ALTER DATABASE [Demo] SET SINGLE_USER
        DROP DATABASE [Demo];
    END
    CREATE DATABASE Demo
    SET NOCOUNT ON;
END

USE Demo;

-- Create a normal table and populate it with data
BEGIN
    CREATE TABLE Member 
    (
        [ID] INT IDENTITY(1,1) NOT NULL,
        [Name] VARCHAR(50) NOT NULL,
        [OpeningBalance] DECIMAL(10,2) NOT NULL,
        [Fees] DECIMAL(10,2) NOT NULL,
        [Paid] DECIMAL(10,2) NOT NULL,
        [Adjustments] DECIMAL(10,2) NOT NULL,
        [ClosingBalance] DECIMAL(10,2) NOT NULL,
        CONSTRAINT [PK_Member] PRIMARY KEY CLUSTERED (ID),
    )  

    INSERT INTO Member (Name, OpeningBalance, Fees, Paid, Adjustments, ClosingBalance)
    VALUES ('Matthew', 0, 100 , 0 , 0, 99),
           ('Mark'   , 0, 1000, 900, 0, 100),
           ('Luke'   , 0, 100 , 50 , 0, 50),
           ('John'   , 0, 100 , 90 , 0, 19)

    SELECT * FROM Member
END        

-- Convert Table to temporal table
BEGIN
    -- We need a columns to track the row lifespan
    ALTER TABLE Member ADD 
    SysStartDate DATETIME2 GENERATED ALWAYS AS ROW START HIDDEN
        CONSTRAINT DF_SysStartDate DEFAULT GETUTCDATE(),
    SysEndDate DATETIME2 GENERATED ALWAYS AS ROW END HIDDEN
        CONSTRAINT DF_SysEndDate DEFAULT CONVERT(DATETIME2, '9999-12-31 23:59:59.9999999'),
    PERIOD FOR SYSTEM_TIME (SysStartDate, SysEndDate)
    
    -- We can now remove the default constraints as we only needed them to valid dates in the existing records
    ALTER TABLE Member DROP CONSTRAINT DF_SysStartDate, DF_SysEndDate
    
    -- Here we enable versioning and we set our history table to MemberAudit
    ALTER TABLE Member SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.MemberAudit));

    SELECT * FROM Member;
    SELECT * FROM MemberAudit;
END 

-- Do some changes on the data
BEGIN
    INSERT INTO Member (Name, OpeningBalance, Fees, Paid, Adjustments, ClosingBalance)
    VALUES ('Paul',    500, 2000 , 1500 , -5, 995);
          
    UPDATE Member
    SET OpeningBalance = OpeningBalance + Fees - Paid + Adjustments,
    Fees = 0,
    Paid = 0,
    Adjustments = 0

    DELETE FROM Member WHERE ID = 1

    DELETE FROM Member WHERE ID = 3
 
    SELECT * FROM Member;
    SELECT * FROM MemberAudit;
END

-- Querying the data
BEGIN 
    SELECT * FROM Member
    SELECT * FROM MemberAudit

    -- Check what the table looked like at a point in time
    SELECT *
    FROM Member 
    FOR SYSTEM_TIME AS OF '2018-09-06 07:53:04.4333333'

    -- Can interact directly with the Audit table if you wish
    -- Find last state of all deleted rows and when they were deleted
    SELECT ID, Name, OpeningBalance, Fees, Paid, Adjustments, ClosingBalance, SysStartDate, SysEndDate
    FROM
    (
        SELECT RowNumber = ROW_NUMBER() OVER (PARTITION BY ma.ID ORDER BY ma.SysStartDate DESC),
        ma.ID, ma.Name, ma.OpeningBalance, ma.Fees, ma.Paid, ma.Adjustments, ma.ClosingBalance, ma.SysStartDate, ma.SysEndDate
        FROM MemberAudit ma
        LEFT JOIN Member m ON ma.ID = m.ID
        WHERE m.ID IS NULL
    ) a
    WHERE RowNumber = 1

    -- Find last state of all updated columns
    SELECT ID, Name, OpeningBalance, Fees, Paid, Adjustments, ClosingBalance, SysStartDate, SysEndDate, NewOpeningBalance, NewFees, NewPaid, NewAdjustments, NewClosingBalance
    FROM
    (
        SELECT RowNumber = ROW_NUMBER() OVER (PARTITION BY ma.ID ORDER BY ma.SysStartDate DESC),
        ma.ID, ma.Name, ma.OpeningBalance, ma.Fees, ma.Paid, ma.Adjustments, ma.ClosingBalance, ma.SysStartDate, ma.SysEndDate,
        m.OpeningBalance AS NewOpeningBalance, m.Fees AS NewFees, m.Paid AS NewPaid, m.Adjustments AS NewAdjustments, m.ClosingBalance AS NewClosingBalance
        FROM MemberAudit ma
        INNER JOIN Member m ON ma.ID = m.ID
    ) a
    WHERE RowNumber = 1
END

-- We can only edit the history table if we disable change tracking
BEGIN
    BEGIN TRANSACTION

    ALTER TABLE Member SET (SYSTEM_VERSIONING = OFF);

    EXEC('DELETE TOP (4) FROM MemberAudit');

    ALTER TABLE Member SET (SYSTEM_VERSIONING = ON (HISTORY_TABLE = dbo.MemberAudit, DATA_CONSISTENCY_CHECK= ON)); 

    -- You can also set the HISTORY_RETENTION_PERIOD if you don't want your temporal history tables growing too large 

    COMMIT TRANSACTION

    SELECT * FROM Member;
    SELECT * FROM MemberAudit;
END




